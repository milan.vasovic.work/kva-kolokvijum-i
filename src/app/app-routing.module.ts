import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllFilmsComponent } from './all-films/all-films.component';
import { FilmComponent } from './film/film.component';
import { EditFilmComponent } from './edit-film/edit-film.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { EditReviewComponent } from './edit-review/edit-review.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { UserComponent } from './user/user.component';
import { AllReviewsComponent } from './all-reviews/all-reviews.component';
import { ReviewComponent } from './review/review.component';

const routes: Routes = [
  {path: "allFilms", component: AllFilmsComponent},
  {path: "film/:id", component: FilmComponent},
  {path: "editFilm/:id", component: EditFilmComponent},
  {path: "allUsers", component: AllUsersComponent},
  {path: "user/:id", component: UserComponent},
  {path: "editUser/:id", component: EditUserComponent},
  {path: "allReviews", component: AllReviewsComponent},
  {path: "review/:id", component: ReviewComponent},
  {path: "editReview/:id", component: EditReviewComponent},
  {path: "**", component: AllFilmsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
