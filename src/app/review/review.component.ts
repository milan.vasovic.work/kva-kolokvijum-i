import { Component, OnInit } from '@angular/core';
import { Review } from '../model/review';
import { ReviewService } from '../service/reviewService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  review: Review = { id : 0, filmId : 0, korisnikId : 0, ocena : 0, komentar : ""};

  constructor(private rs: ReviewService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.rs.getReview(this.activatedRoute.snapshot.params["id"]).subscribe(review => this.review = review);
  }

  getEditReview(id){
    this.router.navigate(["/editReview", id])
  }

}
