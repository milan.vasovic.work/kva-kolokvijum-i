import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/userService';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../model/user';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit {
  allUsers: User[];
  newUser : User = {id : 0, korisnickoIme : "", email : "", lozinka : ""};

  constructor(private us: UserService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.us.getAllUsers().subscribe(allUsers => this.allUsers = allUsers);
  }

  getUser(id){
    this.router.navigate(["/user", id])
  }

  addUser(){
    this.us.addUser(this.newUser).subscribe(r => this.getAll());
  }
  
  removeUser(id){
    this.us.deleteUser(id).subscribe(() => this.getAll());
  }
}
