import { Component, OnInit } from '@angular/core';
import { Film } from '../model/film';
import { FilmService } from '../service/filmService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-all-films',
  templateUrl: './all-films.component.html',
  styleUrls: ['./all-films.component.css']
})
export class AllFilmsComponent implements OnInit {
  allFilms: Film[];
  newFilm : Film = {id : 0, naziv : "", reziser : "", ocena : 0};

  constructor(private fs: FilmService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.fs.getAllFilms().subscribe(allFilms => this.allFilms = allFilms);
  }

  addFilm(){
    this.fs.addFilm(this.newFilm).subscribe(r => this.getAll());
  }

  getFilm(id){
    this.router.navigate(["/film", id])
  }

  removeFilm(id){
    this.fs.deleteFilm(id).subscribe(() => this.getAll());
  }
}
