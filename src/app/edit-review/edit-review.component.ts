import { Component, OnInit } from '@angular/core';
import { Review } from '../model/review';
import { ReviewService } from '../service/reviewService';
import { ActivatedRoute, Router } from '@angular/router';
import { Film } from '../model/film';
import { User } from '../model/user';

@Component({
  selector: 'app-edit-review',
  templateUrl: './edit-review.component.html',
  styleUrls: ['./edit-review.component.css']
})
export class EditReviewComponent implements OnInit {
  allFilms: Film[];
  allUsers: User[];
  review: Review = { id : 0, filmId : 0, korisnikId : 0, ocena : 0, komentar : ""};
  editedReview : Review = { id : 0, filmId : 0, korisnikId : 0, ocena : 0, komentar : ""};
  constructor(private rs: ReviewService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.refres();
    this.getAllFilms();
    this.getAllUsers();
  }

  refres(){
    this.rs.getReview(this.activatedRoute.snapshot.params["id"]).subscribe(review => this.review = review);
  }

  editReview(){
    this.editedReview.ocena = this.review.ocena;
    this.rs.updateReview(this.activatedRoute.snapshot.params["id"], this.editedReview).subscribe(r => this.refres());
  }

  backToReview(id){
    this.router.navigate(["/review", id])
  }
  
  getAllFilms() {
    this.rs.getAllFilms().subscribe(allFilms => this.allFilms = allFilms);
  }

  getAllUsers() {
    this.rs.getAllUsers().subscribe(allUsers => this.allUsers = allUsers);
  }
}
