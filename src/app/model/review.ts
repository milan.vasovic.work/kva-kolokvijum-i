export interface Review{
    id : number,
    filmId : number,
    korisnikId : number,
    ocena : number,
    komentar : string
}