export interface User{
    id : number,
    korisnickoIme : string,
    email : string,
    lozinka : string,
}