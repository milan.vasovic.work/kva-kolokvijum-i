import { Component, OnInit } from '@angular/core';
import { FilmService } from '../service/filmService';
import { Film } from '../model/film';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-film',
  templateUrl: './edit-film.component.html',
  styleUrls: ['./edit-film.component.css']
})
export class EditFilmComponent implements OnInit {
  film: Film = {id : 0, naziv : "", reziser : "", ocena : 0};
  editedFilm : Film = {id : 0, naziv : "", reziser : "", ocena : 0};
  constructor(private fs: FilmService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.refres();
  }

  refres(){
    this.fs.getFilm(this.activatedRoute.snapshot.params["id"]).subscribe(film => this.film = film);
  }

  editFilm(){
    this.editedFilm.ocena = this.film.ocena;
    this.fs.updateFilm(this.activatedRoute.snapshot.params["id"], this.editedFilm).subscribe(r => this.refres());
  }

  backToFilm(id){
    this.router.navigate(["/film", id])
  }
}
