import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { UserService } from '../service/userService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  user: User = {id : 0, korisnickoIme : "", email : "", lozinka : ""};
  editedUser : User = {id : 0, korisnickoIme : "", email : "", lozinka : ""};

  constructor(private us: UserService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.refres();
  }

  refres(){
    this.us.getUser(this.activatedRoute.snapshot.params["id"]).subscribe(user => this.user = user);
  }

  editUser(){
    this.us.updateUser(this.activatedRoute.snapshot.params["id"], this.editedUser).subscribe(r => this.refres());
  }

  backToUser(id){
    this.router.navigate(["/user", id])
  }

}
