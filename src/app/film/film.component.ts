import { Component, OnInit } from '@angular/core';
import { Film } from '../model/film';
import { FilmService } from '../service/filmService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {
  film: Film = {id : 0, naziv : "", reziser : "", ocena : 0};

  constructor(private fs: FilmService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.fs.getFilm(this.activatedRoute.snapshot.params["id"]).subscribe(film => this.film = film);
  }

  getEditFilm(id){
    this.router.navigate(["/editFilm", id])
  }
}
