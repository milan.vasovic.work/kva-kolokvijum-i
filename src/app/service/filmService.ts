import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Film } from '../model/film';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http: HttpClient) {}

  getAllFilms(): Observable<Film[]> {
    return this.http.get<Film[]>("http://localhost:3000/film");
  }

  getFilm(id): Observable<Film> {
    return this.http.get<Film>(`http://localhost:3000/film/${id}`);
  }

  addFilm(newFilm: Film): Observable<Film> {
    return this.http.post<Film>("http://localhost:3000/film", newFilm);
  }
  
  updateFilm(id, film): Observable<Film> {
    return this.http.put<Film>(`http://localhost:3000/film/${id}`, film);
  }

  deleteFilm(id): Observable<Film> {
    return this.http.delete<Film>(`http://localhost:3000/film/${id}`);
  }
}