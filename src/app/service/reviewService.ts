import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Review } from '../model/review';
import { Film } from '../model/film';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  constructor(private http: HttpClient) {}

  getAllReviews(): Observable<Review[]> {
    return this.http.get<Review[]>("http://localhost:3000/ocena");
  }

  getReview(id): Observable<Review> {
    return this.http.get<Review>(`http://localhost:3000/ocena/${id}`);
  }

  addReview(review: Review): Observable<Review> {
    return this.http.post<Review>("http://localhost:3000/ocena", review);
  }
  
  updateReview(id, review): Observable<Review> {
    return this.http.put<Review>(`http://localhost:3000/ocena/${id}`, review);
  }

  deleteReview(id): Observable<Review> {
    return this.http.delete<Review>(`http://localhost:3000/ocena/${id}`);
  }

  getAllFilms(): Observable<Film[]> {
    return this.http.get<Film[]>("http://localhost:3000/film");
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>("http://localhost:3000/korisnik");
  }
}
