import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>("http://localhost:3000/korisnik");
  }

  getUser(id): Observable<User> {
    return this.http.get<User>(`http://localhost:3000/korisnik/${id}`);
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>("http://localhost:3000/korisnik", user);
  }
  
  updateUser(id, user): Observable<User> {
    return this.http.put<User>(`http://localhost:3000/korisnik/${id}`, user);
  }

  deleteUser(id): Observable<User> {
    return this.http.delete<User>(`http://localhost:3000/korisnik/${id}`);
  }
}