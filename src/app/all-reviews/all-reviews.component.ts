import { Component, OnInit } from '@angular/core';
import { Review } from '../model/review';
import { ReviewService } from '../service/reviewService';
import { ActivatedRoute, Router } from '@angular/router';
import { Film } from '../model/film';
import { User } from '../model/user';

@Component({
  selector: 'app-all-reviews',
  templateUrl: './all-reviews.component.html',
  styleUrls: ['./all-reviews.component.css']
})
export class AllReviewsComponent implements OnInit {
  allReviews: Review[];
  allFilms: Film[];
  allUsers: User[];
  newReview : Review = { id :0, filmId : 0, korisnikId : 0, ocena : 0, komentar : ""};

  constructor(private rs: ReviewService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.getAll();
    this.getAllFilms();
    this.getAllUsers();
  }

  getAll() {
    this.rs.getAllReviews().subscribe(allReviews => this.allReviews = allReviews);
  }
  
  getAllFilms() {
    this.rs.getAllFilms().subscribe(allFilms => this.allFilms = allFilms);
  }

  getAllUsers() {
    this.rs.getAllUsers().subscribe(allUsers => this.allUsers = allUsers);
  }

  addReview(){
    this.rs.addReview(this.newReview).subscribe(r => this.getAll());
  }

  getReview(id){
    this.router.navigate(["/review", id])
  }

  removeReview(id){
    this.rs.deleteReview(id).subscribe(() => this.getAll());
  }
}
