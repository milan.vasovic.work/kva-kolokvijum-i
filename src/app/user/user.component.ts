import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { UserService } from '../service/userService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User = {id : 0, korisnickoIme : "", email : "", lozinka : ""};

  constructor(private us: UserService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.us.getUser(this.activatedRoute.snapshot.params["id"]).subscribe(user => this.user = user);
  }

  getEditUser(id){
    this.router.navigate(["/editUser", id])
  }
}
